# Kimono Hugo Theme

## About

Kimono is a set of themes for Hugo Tool.

This repository is the dev version. You can clone it to customise the theme.



## Use

In your project root place, go to the theme directory

    $ cd themes

And clone the theme

    $ git clone ...

In your project config file, replace the theme option.

## Edit

Clone the repo

    $ git clone ...

Install the dependencies

    $ npm install

Build and bundle it

    $ npm run build

To have the livereload, you can type

    $ npm run build:watch