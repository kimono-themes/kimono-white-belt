const path = require("path");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env, argv) => { 
  const devMode = argv.mode !== 'production';
  console.log('Mode', argv.mode );
  return {

    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: path.resolve(__dirname, 'static')
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                  'style-loader',
                  'css-loader', 
                  'sass-loader'
                ]
            }
        ]
    },
    plugins: []
    }
};